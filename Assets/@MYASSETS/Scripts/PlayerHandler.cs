using DilmerGames.Core.Singletons;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerHandler : NetworkSingleton<PlayerHandler>
{
    private NetworkVariable<int> playerCounter = 
        new NetworkVariable<int>();

    public int PlayerCounter
    { 
        get
        { 
            return playerCounter.Value; 
        }
    }
    // Start is called before the first frame update
    void Start()
    {

        NetworkManager.Singleton.OnClientConnectedCallback += ClientConnected;
        NetworkManager.Singleton.OnClientDisconnectCallback += ClientDisConnected;
        //NetworkManager.Singleton.OnClientConnectedCallback += (id) =>
        //{
        //    if (IsServer)
        //    {
        //        Logger.Instance.LogInfo("Client " + id + " connected");
        //        playerCounter.Value++;
        //    }
        //};
        //NetworkManager.Singleton.OnClientDisconnectCallback += (id) =>
        //{
        //    if (IsServer)
        //    {
        //        Logger.Instance.LogInfo("Client " + id + " disconnected");
        //        playerCounter.Value--;
        //    }
        //};

    }


    private void ClientConnected(ulong _num)
    {
        if (IsServer) 
        {  
            Logger.Instance.LogInfo("Client " + _num + " connected");
            playerCounter.Value++;
        }
       
    }

    private void ClientDisConnected(ulong _num)
    {
        if (IsServer)
        {
            Logger.Instance.LogInfo("Client " + _num + " disconnected");
            playerCounter.Value--;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
