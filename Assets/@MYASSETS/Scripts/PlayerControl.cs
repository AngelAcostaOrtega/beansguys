using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerControl : NetworkBehaviour
{
    private NetworkVariable<Vector3> netMovement = new NetworkVariable<Vector3>();
    private NetworkVariable<Vector3> netRotation = new NetworkVariable<Vector3>();

    public enum PlayerStates
    {
        WalkingBack,
        Idle,
        Walking,
        Running
    };

    private NetworkVariable<PlayerStates> netState = new NetworkVariable<PlayerStates>();

    private CharacterController characterController = null;
    private Animator animator = null;

    private Vector3 RandomVector { get { return this.RandomizePosition(); } }

    [SerializeField][Range(1f, 10f)]
    private float moveSpeed = 10f;
    [SerializeField][Range(1f, 10f)]
    private float turnSpeed = 6f;

    private void Awake()
    {
        if (this.TryGetComponent(out Animator anim)) animator = anim;
        if (this.TryGetComponent(out CharacterController cc)) characterController = cc;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(IsOwner)
        {
            MoveAndRotate(RandomVector, Vector3.zero);
            this.transform.Find("Camera").gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (netMovement.Value != null) characterController.SimpleMove(netMovement.Value);
        if (netRotation.Value != null) transform.Rotate(netRotation.Value);

        ChangeAnimation();

        if(IsClient && IsOwner)
        {
            float forwardInput = Input.GetAxis("Vertical");
            if(Input.GetKey(KeyCode.LeftShift) && forwardInput > 0)
            {
                forwardInput *= 2; // CORRAN
            }

            float turnInput = Input.GetAxis("Horizontal") / 2;
            MoveAndRotate(transform.forward * forwardInput * moveSpeed, new Vector3(0, turnInput * turnSpeed, 0));


            if( forwardInput == 0 )
            {
                UpdateState(PlayerStates.Idle);
            }
            else if(forwardInput >= -1 && forwardInput <= 1)
            {
                if(forwardInput > 0)
                {
                    UpdateState(PlayerStates.Walking);
                }
                else
                {
                    UpdateState(PlayerStates.WalkingBack);
                }
            }
            else
            {
                UpdateState(PlayerStates.Running);
            }
        }
    }

    private void UpdateState(PlayerStates state)
    {
        if(IsServer)
        {
            netState.Value = state;
        }
        else
        {
            UpdateStateServerRpc(state);
        }
    }

    [ServerRpc]
    private void UpdateStateServerRpc(PlayerStates state)
    {
        netState.Value = state;
    }

    private void MoveAndRotate(Vector3 move, Vector3 rotate)
    {
        if(IsServer)
        {
            netMovement.Value = move;
            netRotation.Value = rotate;
        }
        else
        {
            MoveAndRotateServerRpc(move, rotate);
        }
    }
    [ServerRpc]
    private void MoveAndRotateServerRpc(Vector3 move, Vector3 rotate)
    {
        netMovement.Value = move;
        netRotation.Value = rotate;
    }

    private Vector3 RandomizePosition()
    {
        return new Vector3(Random.Range(-4, 4),
                           0,
                           Random.Range(-4, 4));
    }

    private void ChangeAnimation()
    {
        if(animator != null)
        {
            switch (netState.Value)
            {
                case PlayerStates.WalkingBack:
                    animator.SetInteger("Walk", -1);
                    break;
                case PlayerStates.Idle:
                    animator.SetInteger("Walk", 0);
                    break;
                case PlayerStates.Walking:
                    animator.SetInteger("Walk", 1);
                    break;
                case PlayerStates.Running:
                    animator.SetInteger("Walk", 2);
                    break;
                default:
                    break;
            }
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit col)
    {
        if(col.gameObject.CompareTag("WinBox"))
        {
            Logger.Instance.LogInfo("Player " + this.NetworkObject.name + " has win.");
            col.gameObject.GetComponent<Collider>().enabled = false;

            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            gameManager.finishGame();

            Debug.Log("Llamado a " + gameManager.name);
        }
    }
}
