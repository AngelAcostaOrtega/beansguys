using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class GameManager : NetworkBehaviour
{
    [SerializeField]
    private GameObject panel = null;

    private NetworkVariable<bool> endGame = new NetworkVariable<bool>();

    private void OnEnable()
    {
        endGame.OnValueChanged += OnEndGame;
    }
    private void OnDisable()
    {
        endGame.OnValueChanged -= OnEndGame;
    }

    public void finishGame()
    {
        if (IsServer)
        {
            endGame.Value = true;
        }
        else if(IsOwner)
        {
            finishGameServerRpc();
        }
    }
    [ServerRpc]
    private void finishGameServerRpc()
    {
        endGame.Value = true;
    }

    private void OnEndGame(bool previousValue, bool newValue)
    {
        if(endGame.Value)
        {
            panel.transform.parent.Find("Logger").GetChild(0).gameObject.SetActive(false);
            panel.transform.GetChild(0).gameObject.SetActive(true);
        }
    }
}
