using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public void StartServer()
    {
        if( NetworkManager.Singleton.StartServer() )
        {
            Logger.Instance.LogInfo("Server starter...");
            GameObject.Find("Canvas/Botones").transform.GetChild(0).gameObject.SetActive(false);
            GameObject.Find("Canvas/Logger").transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            Logger.Instance.LogInfo("Unable to start server...");
        }
    }
    public void StartHost()
    {
        if (NetworkManager.Singleton.StartHost())
        {
            Logger.Instance.LogInfo("Host starter...");
            GameObject.Find("Canvas/Botones").transform.GetChild(0).gameObject.SetActive(false);
            GameObject.Find("Canvas/Logger").transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            Logger.Instance.LogInfo("Unable to start host...");
        }
    }
    public void StartClient()
    {
        if (NetworkManager.Singleton.StartClient())
        {
            Logger.Instance.LogInfo("Client starter...");
            GameObject.Find("Canvas/Botones").transform.GetChild(0).gameObject.SetActive(false);
            GameObject.Find("Canvas/Logger").transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            Logger.Instance.LogInfo("Unable to start client...");
        }
    }
}
